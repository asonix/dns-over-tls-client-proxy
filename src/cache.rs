use async_mutex::Mutex;
use std::{net::SocketAddr, sync::Arc, time::Duration};
use ttl_cache::TtlCache;

const REQ_LIMIT: Duration = Duration::from_secs(2);

#[derive(Clone)]
pub struct RequestCache {
    inner: Arc<Mutex<TtlCache<[u8; 2], SocketAddr>>>,
}

impl RequestCache {
    pub fn new() -> Self {
        RequestCache {
            inner: Arc::new(Mutex::new(TtlCache::new(1024))),
        }
    }

    pub async fn insert(&self, key: &[u8], value: SocketAddr) {
        if key.len() < 2 {
            return;
        }
        self.inner
            .lock()
            .await
            .insert([key[0], key[1]], value, REQ_LIMIT);
    }

    pub async fn remove(&self, key: &[u8]) -> Option<SocketAddr> {
        if key.len() < 2 {
            return None;
        }
        self.inner.lock().await.remove(&[key[0], key[1]])
    }
}

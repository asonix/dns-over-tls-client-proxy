use bytes::BytesMut;
use futures::{
    future::try_join,
    sink::SinkExt,
    stream::{select, select_all, FuturesUnordered, StreamExt},
};
use log::{debug, error, info, warn};
use std::{
    io,
    net::SocketAddr,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};
use structopt::StructOpt;
use tokio::{net::UdpSocket, sync::mpsc::channel};
use tokio_rustls::{rustls::ClientConfig, webpki::DNSNameRef};
use tokio_stream::wrappers::ReceiverStream;
use tokio_util::{codec::BytesCodec, udp::UdpFramed};
use webpki_roots::TLS_SERVER_ROOTS;

mod cache;
mod conn;

static TEST_QUERY: &[u8] = &[
    1, 32, 0, 1, 0, 0, 0, 0, 0, 1, 11, 100, 111, 117, 98, 108, 101, 99, 108, 105, 99, 107, 3, 110,
    101, 116, 0, 0, 1, 0, 1, 0, 0, 41, 16, 0, 0, 0, 0, 0, 0, 12, 0, 10, 0, 8, 126, 55, 17, 213,
    219, 230, 65, 120,
];

fn build_test_query() -> BytesMut {
    use rand::RngCore;
    let mut id = vec![0u8; 2];
    rand::thread_rng().fill_bytes(&mut id);
    id.extend(TEST_QUERY);

    let mut bm = BytesMut::new();
    bm.extend(id);
    bm.extend(TEST_QUERY);
    bm
}

#[derive(StructOpt)]
struct Options {
    #[structopt(short, long, help = "The local address to bind the server to")]
    address: Option<SocketAddr>,

    #[structopt(short, long, help = "The IP of the upstream DNS server")]
    upstream: SocketAddr,

    #[structopt(short, long, help = "Fallback plaintext DNS server")]
    fallback: SocketAddr,

    #[structopt(short, long, help = "The domain of the upstream server")]
    domain: String,
}

#[tokio::main]
async fn main() -> Result<(), anyhow::Error> {
    let mut options = Options::from_args();

    let fallback = options.fallback;
    let local_address = options.address.take().unwrap_or("127.0.0.1:53".parse()?);
    let domain = DNSNameRef::try_from_ascii_str(&options.domain)?.to_owned();

    env_logger::init();

    let mut config = ClientConfig::new();
    config
        .root_store
        .add_server_trust_anchors(&TLS_SERVER_ROOTS);

    let udp_socket = UdpSocket::bind(local_address).await?;
    info!("Listening on {}", local_address);
    let udp_framed = UdpFramed::new(udp_socket, BytesCodec::new());

    let (mut udp_sink, udp_stream) = udp_framed.split();

    let use_fallback = Arc::new(AtomicBool::new(false));
    let config = self::conn::Config::new(
        config,
        options.upstream,
        domain,
        use_fallback.clone(),
        fallback,
    );

    let (ctrl_cmd_tx, ctrl_cmd_rx) = channel(8);
    let (ctrl_dns_tx, mut ctrl_dns_rx) = channel(8);

    let mut unordered = FuturesUnordered::new();
    let mut txs = vec![];
    let mut rxs = vec![];

    for _ in 0..16 {
        let (tx1, rx1) = channel(32);
        let (tx2, rx2) = channel(32);
        unordered.push(tokio::spawn(self::conn::run(config.clone(), rx1, tx2)));
        txs.push(tx1);
        rxs.push(ReceiverStream::new(rx2));
    }

    let any_addr = "0.0.0.0:0".parse::<SocketAddr>().unwrap();

    let ctrl_fut = tokio::spawn(async move {
        use tokio::time::{interval, sleep, timeout, Duration};

        let mut i = interval(Duration::from_secs(5));

        let max_fails: usize = 5;
        let mut fail_count = 0;

        loop {
            if fail_count == 0 {
                i.tick().await;
            } else if fail_count == max_fails {
                error!("Reached max failures, falling back to plaintext");
                use_fallback.store(true, Ordering::Relaxed);
                sleep(Duration::from_secs(60)).await;
                i = interval(Duration::from_secs(5));
            }

            let _ = ctrl_cmd_tx.send(Ok((build_test_query(), any_addr))).await;

            if let Err(_) = timeout(Duration::from_secs(2), ctrl_dns_rx.recv()).await {
                warn!("Failed to get response for test query");
                fail_count += 1;
            } else {
                if use_fallback.load(Ordering::Relaxed) {
                    info!("TLS server back online, disabling fallback");
                    use_fallback.store(false, Ordering::Relaxed);
                }
                fail_count = 0;
            }
        }
    });

    let handler_fut = tokio::spawn(async move {
        while let Some(_) = unordered.next().await {
            warn!("Handler completed");
            // pass
        }
        Ok(()) as Result<(), io::Error>
    });

    let mut rx_stream = select_all(rxs);

    let reply_fut = async move {
        while let Some(tup) = rx_stream.next().await {
            if tup.1 == any_addr {
                let _ = ctrl_dns_tx.send(tup.0).await;
                continue;
            }
            debug!("Responding to {}", tup.1);

            udp_sink.send(tup).await?;
        }
        Ok(()) as Result<(), io::Error>
    };

    let request_fut = async move {
        let mut idx = 0;
        let max_idx = txs.len();
        let ctrl_cmd_rx_stream = ReceiverStream::new(ctrl_cmd_rx);
        let mut stream = select(udp_stream, ctrl_cmd_rx_stream);
        while let Some(res) = stream.next().await {
            let (bytes, addr) = res?;

            debug!("Requesting for {}", addr);

            let tup = (bytes.freeze(), addr);

            if let Err(_) = txs[idx].send(tup).await {
                error!("Unexpected dropped receiver!!!");
            }

            idx = (idx + 1) % max_idx;
        }
        Ok(()) as Result<(), io::Error>
    };

    let udp_fut = tokio::spawn(try_join(reply_fut, request_fut));

    let (r1, r2, _) = futures::try_join!(handler_fut, udp_fut, ctrl_fut)?;
    r1?;
    r2?;

    Ok(())
}

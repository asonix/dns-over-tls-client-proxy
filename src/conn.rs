use crate::cache::RequestCache;
use bytes::Bytes;
use futures::{sink::SinkExt, stream::StreamExt, try_join};
use log::{debug, error};
use std::{
    io,
    net::SocketAddr,
    sync::{
        atomic::{AtomicBool, Ordering},
        Arc,
    },
};
use tokio::{
    net::{TcpStream, UdpSocket},
    sync::mpsc::{Receiver, Sender},
};
use tokio_rustls::{client::TlsStream, rustls::ClientConfig, webpki::DNSName, TlsConnector};
use tokio_util::{
    codec::{length_delimited::LengthDelimitedCodec, BytesCodec, Framed},
    udp::UdpFramed,
};

#[derive(Clone)]
pub struct Config {
    config: Arc<ClientConfig>,
    upstream: SocketAddr,
    domain: DNSName,
    use_fallback: Arc<AtomicBool>,
    fallback: SocketAddr,
    cache: RequestCache,
}

pub type Conn = Framed<TlsStream<TcpStream>, LengthDelimitedCodec>;

impl Config {
    pub fn new(
        config: ClientConfig,
        upstream: SocketAddr,
        domain: DNSName,
        use_fallback: Arc<AtomicBool>,
        fallback: SocketAddr,
    ) -> Self {
        Config {
            config: Arc::new(config),
            upstream,
            domain,
            use_fallback,
            fallback,
            cache: RequestCache::new(),
        }
    }

    pub async fn connect(&self) -> Result<Conn, io::Error> {
        let stream = TcpStream::connect(&self.upstream).await?;
        let connector = TlsConnector::from(self.config.clone());

        let tls_stream = connector.connect(self.domain.as_ref(), stream).await?;

        let framed = LengthDelimitedCodec::builder()
            .length_field_length(2)
            .new_framed(tls_stream);

        Ok(framed)
    }
}

pub async fn run(
    config: Config,
    mut rx: Receiver<(Bytes, SocketAddr)>,
    tx: Sender<(Bytes, SocketAddr)>,
) {
    while let Err(e) = do_run(config.clone(), &mut rx, tx.clone()).await {
        error!("Error in forwarder, {}", e);
    }
}

async fn do_run(
    config: Config,
    rx: &mut Receiver<(Bytes, SocketAddr)>,
    tx: Sender<(Bytes, SocketAddr)>,
) -> Result<(), io::Error> {
    let any_addr = "0.0.0.0:0".parse::<SocketAddr>().unwrap();

    let fallback_socket = UdpSocket::bind(any_addr).await?;
    let fallback_framed = UdpFramed::new(fallback_socket, BytesCodec::new());
    let (mut fallback_sink, mut fallback_stream) = fallback_framed.split();
    let fallback = config.fallback;

    let conn = config.connect().await?;
    let (mut sender, mut receiver) = conn.split();

    let cache = config.cache.clone();
    let use_fallback = config.use_fallback.clone();
    let f1 = async move {
        while let Some((bytes, addr)) = rx.recv().await {
            debug!(
                "Inserting {} for {}{}",
                addr,
                bytes.as_ref()[0],
                bytes.as_ref()[1]
            );
            cache.insert(bytes.as_ref(), addr).await;

            if use_fallback.load(Ordering::Relaxed) && addr != any_addr {
                fallback_sink.send((bytes, fallback)).await?;
            } else {
                sender.send(bytes).await?;
            }
        }
        Ok(()) as Result<(), io::Error>
    };

    let cache = config.cache.clone();
    let tx2 = tx.clone();
    let f2 = async move {
        while let Some(res) = receiver.next().await {
            let bytes = res?.freeze();

            if let Some(addr) = cache.remove(bytes.as_ref()).await {
                if let Err(_) = tx2.send((bytes, addr)).await {
                    error!("Error replying to {}", addr);
                }
            } else {
                debug!(
                    "Couldn't find addr for {}{}",
                    bytes.as_ref()[0],
                    bytes.as_ref()[1]
                );
            }
        }
        Ok(()) as Result<(), io::Error>
    };

    let cache = config.cache.clone();
    let f3 = async move {
        while let Some(res) = fallback_stream.next().await {
            let (bytes, _) = res?;
            let bytes = bytes.freeze();

            if let Some(addr) = cache.remove(bytes.as_ref()).await {
                if let Err(_) = tx.send((bytes, addr)).await {
                    error!("Error replying to {}", addr);
                }
            } else {
                debug!(
                    "Couldn't find addr for {}{}",
                    bytes.as_ref()[0],
                    bytes.as_ref()[1]
                );
            }
        }
        Ok(()) as Result<(), io::Error>
    };

    try_join!(f1, f2, f3)?;
    Ok(())
}

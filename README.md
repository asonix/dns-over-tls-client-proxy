# DNS over TLS client
_a simple client proxy for DoT queries_

### Usage
#### Run
```
$ ./dns-over-tls-client --help
dns-over-tls-client 0.1.0

USAGE:
    dns-over-tls-client [OPTIONS] --domain <domain> --upstream <upstream>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -a, --address <address>      The local address to bind the server to
    -d, --domain <domain>        The domain of the upstream server
    -u, --upstream <upstream>    The upstream DNS server
```

##### Example:
Run it
```
$ ./dns-over-tls -a 0.0.0.0:8053 -d dns.asonix.dog -u 24.55.3.111
```
Use it
```
$ dig @localhost -p 8053 asonix.dog
```

DNS over TLS client respects the `RUST_LOG` environment variable for setting log levels. Running
with `RUST_LOG=info` is recommended.

### Contributing
Unless otherwise stated, all contributions to this project will be licensed under the CSL with
the exceptions listed in the License section of this file.

### License
This work is licensed under the Cooperative Software License. This is not a Free Software
License, but may be considered a "source-available License." For most hobbyists, self-employed
developers, worker-owned companies, and cooperatives, this software can be used in most
projects so long as this software is distributed under the terms of the CSL. For more
information, see the provided LICENSE file. If none exists, the license can be found online
[here](https://lynnesbian.space/csl/). If you are a free software project and wish to use this
software under the terms of the GNU Affero General Public License, please contact me at
[asonix@asonix.dog](mailto:asonix@asonix.dog) and we can sort that out. If you wish to use this
project under any other license, especially in proprietary software, the answer is likely no.
